// src/components/Attacks.js

import React from 'react'

let Attacks = ({ attacks }) => {

    return (
        <div>
            {attacks.map((attack) => (
                <div className="attack">
                    <h6 className="card-text">Attack : {attack.name} | Cost : {attack.cost} </h6>
                    <h6>{attack.text}</h6>
                    <h6 className="card-text">Damage : {attack.damage} | Energy : {attack.convertedEnergyCost}</h6>
                </div>
            )
            )}
        </div>
    )
};

export default Attacks