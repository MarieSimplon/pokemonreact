// src/components/Abilities.js

import React from 'react'

let Abilities = ({ abilities }) => {

    return (
        <div>
            {abilities.map((ability => (
                <div className="ability">
                    <h6 className="card-text">Ability : {ability.name} | {ability.text} | Type : {ability.type}</h6>
                </div>
            )
            ))}
        </div>
    )
};

export default Abilities