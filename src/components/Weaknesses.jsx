// src/components/Weaknesses.js

import React from 'react'

let Weaknesses = ({ weaknesses }) => {

    return (
        <div>
            {weaknesses.map((weakness) => (
                <div className="weakness">
                    <h6 className="card-text">Weakness type : {weakness.type} | Value : {weakness.value}</h6>
                </div>
            ))}
        </div>
    )
}

export default Weaknesses
