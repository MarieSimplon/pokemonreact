// src/components/Resistances.js

import React from 'react'

let Resistances = ({ resistances }) => {
    return (
        <div>
            {resistances.map((resistance) => (
                <div className="resistance">
                    <h6 className="card-text">Resistance Name : {resistance.type} | Value : {resistance.value}</h6>
                </div>
            )
            )}
        </div>
    )
};

export default Resistances

