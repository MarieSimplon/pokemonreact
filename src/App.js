import React, { Component } from 'react';

import Abilities from './components/Abilities';
import Attacks from './components/Attacks';
import Weaknesses from './components/Weaknesses';
import Resistances from './components/Resistances';

class App extends Component {
    constructor() {
        super();
        this.state = {
            query: "",
            data: [],
            filteredData: []
        }
    }
    //gère le changement
    handleInputChange = event => {
        const query = event.target.value;

        this.setState(prevState => {
            const filteredData = prevState.data.cards.filter(element => {
                return element.name.toLowerCase().includes(query.toLowerCase());
            });
            return {
                query,
                filteredData
            };
        });
    };

    //récupère les informations via le fetch (affiche toutes les cartes & le tri)
    getData = () => {
        fetch(`https://api.pokemontcg.io/v1/cards?pageSize=1000`)
            .then(response => response.json())
            .then(data => {
                const { query } = this.state;
                const filteredData = data.cards.filter(element => {
                    return element.name.toLowerCase().includes(query.toLowerCase())
                });

                this.setState({
                    data,
                    filteredData
                });
            });
    };

    //récupère le résultat
    componentWillMount() {
        this.getData();
    }

    render() {
        return (
            <React.Fragment>
                <img className="mx-auto d-block pokeindex" alt="Pokemon" src={require('./img/index-pokemon.png')} />
                <div className="container">
                    <div className="row">

                        <div className="col-10 mx-auto col-md-8 text-center">
                            <h5>Find your Pokemon Card !</h5>
                            <form className="form align-items">
                                <input
                                    className="form-control"
                                    placeholder="Snorlax, Xerneas, Aggron..."
                                    value={this.state.query}
                                    onChange={this.handleInputChange}
                                />
                            </form>
                        </div>
                        <div className="row">
                            <div className="container">
                                {this.state.filteredData.map(i =>
                                    <div class="row pika" key={i.id}>

                                        <div className="col-8 mx-auto col-md-6">
                                            <img
                                                src={i.imageUrl}
                                                alt={i.name}
                                            />
                                            <h6 className="mx-auto">Illustrator : {i.artist}</h6>
                                        </div>

                                        <div class="col-8 mx-auto col-md-6 align-self-center">
                                            <h5 class="">{i.name}</h5>
                                            <h6 className="">Id : {i.id}</h6>
                                            <h6 className="">Type : {i.supertype} |
                                                Genre : {i.types} |
                                                Subtype : {i.subtype} |
                                                Hp : {i.hp}

                                            </h6>
                                            <h6 className="">
                                                Rarity : {i.rarity} |
                                                Series : {i.series} |
                                                Set : {i.set} |

                                                {i.abilities &&
                                                    <Abilities abilities={i.abilities} />
                                                }
                                                {i.attacks &&
                                                    <Attacks attacks={i.attacks} />
                                                }
                                                {i.resistances &&
                                                    <Resistances resistances={i.resistances} />
                                                }
                                                {i.weaknesses &&
                                                    <Weaknesses weaknesses={i.weaknesses} />
                                                }
                                            </h6>
                                        </div>

                                        <br></br>

                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

export default App;